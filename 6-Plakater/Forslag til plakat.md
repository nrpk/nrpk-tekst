## Kjære markavenn

#### Velkommen til Nøklevann Ro og Padleklubb (NRPK)

Vi er et idrettslag organisert under **Norges idrettsforbund**, hvor du som medlem har fri tilgang på alt av klubbens materiell ved åpningstidene. 

For å bli medlem må du først melde deg på **NRPK's introduksjonskurs** (300,- for alle over 12 år), så må du for hvert år du vil beholde medlemskapet **betale klubbens årskontingent** som nå er kr 275,- for voksne og kr 100,- for barn (under 12 år). Årskontingent gjelder for en sesong som begynner i Mai og slutter i Oktober.

For dette får du tilgang til alt av klubbens utstyr, bestående av **kajakk**, **kano**, **sup** og tradisjonell **robåt** som passer fra nybegynnere til erfarne padlere. Du har selvfølgelig også tilgang på redningsvester og padleårer.

Tilbudet er altså for de som har **fullført introduksjonskurs** og som har **betalt den årlige kontingenten.**

Introduksjonskurset viser deg hvordan klubben fungerer, hvordan du skal behandle alt av klubbens materiell og hvordan du skal opptre på vannet.

Barn under 12 år kan ikke padle uten følge av voksne. Materiellet er kun for medlemmer; du kan <u>ikke</u> dele kano med ikke-medlemmer.

Våre medlemmer er ved bruk av klubbens utstyr forsikret i **IF skadeforsikring,** såfremt de følger reglementet som blant annet sier at alle som benytter klubbens utstyr **må bruke redningsvest.**

Du kan også få råd og hjelp av våre vakter. Ved finvær er det som regel stor pågang og bruken av utstyr begrenser seg da til **3 timer**.

---

#### Bryggen

Bryggen tilhører primært klubben og er ment for medlemmers på– og avstigning fra leide båter. På varme solrike dager kan publikum allikevel få tilgang til en begrenset del av bryggen, tydelig markering er gjort på de stedene hvor bryggen er utilgjengelig for publikum som ikke er medlemmer i klubben.

Vis hensyn – neste gang er det kanskje du som sitter i en ustø kajakk og da er det fint at omgivelsene er rolige.

#### Muligheter

Du kan overnatte i marka med klubbens utstyr så lenge du henter det ut før stengetid, og leveres innen åpningstid neste virkedag.

Klubben har nominelt åpent lørdag-søndag 11-17 og tirsdag-onsdag-torsdag 18-21. Tidene begrenses noe mot slutten av sesongen.

Vi har dugnad ved sesongstart og sesongavslutning – medlemmer oppfordres til å delta på dugnad.

---

Mer om dette kan du lese på klubbens hjemmeside, www.nrpk.no.