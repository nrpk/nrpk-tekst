# Kopiert fra [Vahid](), Vaktrutine:

## 1. Oppsummert

NRPKs nøkkelvakter er fremoverlente, engasjerte, sikkerhetsorienterte serviceinnstilte, vennlige hjelpere og problemløsere med god oversikt over klubbens utstyr, rutiner!

Nøkkel-vaktene kommer forberedt til oppgaven, planlegger godt og følger aktivt med og det som skjer og styrer aktivitetene til beste for alle*, *samt avslutter sin vakt med å legge forholdene godt til rette for neste vakter!

> Møte forberedt og i god tid- Planlegge sammen – Kommunisere aktiv under vakten – Være tilstede – Styre begivenhetene når nødvendig – Avslutte ryddig og skikkelig!

## 2. Før oppmøte 
Vi anbefaler alle til å ha en gjennomgang av denne siden dagen, eller noen timer før de skal på vakt. Dette fordi:

- Rutiner og måten vi utfører oppgavene på kan forsvinne i bakhodet når det er gått en stund siden forrige vakt.
- Det er generelt godt å være mentalt forberedt på oppgavene.
- Det kan være nyheter, beskjeder og endringer i rutiner og aktiviteter du må forholde deg til. Disse blir publisert her.
- Det kan også være dagsaktuelle opplysninger som du vil finne under boksen **«Dagsaktuelt»**
- Det kan også være nødvendig at du frisker opp kunnskapen du fikk med deg på nøkkelvakt-kurs/møter, med å lese gjennom Nøkkelvakt-sidene.

## 3. Oppmøte
Møt opp i god tid (helst en halvtime/ senest et kvarter) før åpningstid.
-	Forberedelser og planlegging er viktig for å ha en vellykket vakt.  Når brukerne kommer, kan det bli lite tid til nødvendige forberedelser.
-	Snakk sammen og bli enige om arbeidsdeling. Husk:
-	«Slik vi gjorde sist» forstås ikke likt for vakter som ikke var sammen sist.
-	Sørg for at flere ikke er på samme oppgaver, mens enkelte oppgaver ikke er dekt.
-	En del av oppgaver må helst være fullført før første bruker er der.  Sørg for at oppslagstavlen / whiteboard inneholder nødvendig informasjon om fristen for at brukere skal være inne og andre nødvendig  informasjon. Se til at tavlen ikke inneholder andre informasjon enn de  mest nødvendige beskjedene.
-	Disponer tiden og ressursene, slik at  dere har god kontroll på både avkryssingsoppgaver, betjening av brukere  samt kontroll/ påse-oppgaver. Alle oppgaver i skjemaet må være fordelt.

## 4. Under vaktøkt

Forhold dere til arbeidsdelingen dere er blitt enige om.
-	Forlat ikke posten/ oppgavene, før du er blitt enig om endringer med andre vakter.
-	Henvis brukere som spør om hjelp til vakten som har den aktuelle oppgaven.
-	Med flere samtidige oppgaver/ henvendelser, ha en prioritering. Be folk høflig om å vente ved behov.
-	Eksempelvis kan arbeidsdelingen være at en/ to vakt(er) hjelper brukere og passer på trafikken på brygga, mens en annen:
-	Sjekker medlemskort.
-	påser at alle brukere og utstyr blir loggført
-	at alle har vest på seg før de er ute på vannet.
-	oppfordrer brukere til å lese oppslagstavla (whiteboard) før de starter turen.
-	Det er en viktig oppgave å ha kontroll på **trafikken på brygga**. Dette gjelder både de som skal ut, de som kommer tilbake og ikke minst publikum som bruker brygga til svømming/ soling. Gi nødvendig råd, veiledning og om nødvendig klar beskjed om kjøreregler til de som trenger det på brygga.
-	Det er **absolutt ikke ønskelig** at vakter tar seg en padle-/rotur fordi det er få brukere. **Er man på vakt, så er man på vakt!** Er det en rolig dag, kan man alltid finne på en del oppgaver som til vanlig ikke blir tid til (se under «Aktiviteter for rolige dager» eller gjennomgå/rydde utstyr/vaktrommet!

## 5. Avslutning av vaktøkt

- Etter at du har sjekket at alle medlemmer som er ute på vannet og skulle ha kommet tilbake (de uten nøkkel) er tilbake, ha en varetelling  av alt utlånt utstyr (båter, årer,…). Sørg for at alt er på rett plass.
- Gjennomgå og sett alt i den stand du selv ville ha mottatt ved  starten av vakten. Dette gjelder spesielt de administrative verktøyene.
- Meld om mangler og feil og oppgavene som kan gjøres på «rolige» dager.
- Meld også om du har utført noe ekstra arbeid/ aktiviteter  (opprydding, reparasjon eller forefallende fra boksen «Aktiviteter for  rolige dager») i løpet av vakten.
- Er det noe de neste vaktene må vite? Send i så fall en melding til vakt@nrpk.no med kopi til *( her skal vi spesifisere en e-post / sms til den som oppdaterer boksen dagsaktuell)*. **Husk å inkludere følgende i påminnelsen du sender til neste vakter:** » Vi ber om at du leser gjennom følgende web-side før du skal på vakt: [nrpk.no/vaktrutine](http://nrpk.no/vaktrutine).»

## 6. Dagsaktuelt

Her kommer de viktige tidsaktuelle beskjedne til vakter, inkludert forslag

- Ha en god vakt. 

## 7. Aktiviteter for rolige dager!
Er det en rolig dag med for lite å gjøre?

- I denne boksen finner du en liste med oppgaver som kan utføres på rolige dager/ når det er lite trafikk.
- Du kan også foreslå oppgaver som kan settes på denne lista med å sende en melding til [vakt@nrpk.no](vakt@nrpk.no)

