# Kopiert fra [Ulf](), vaktinstruks

## 1. Oppsummert

Nøkkelvakten viktigste oppgave er å ved sine handlinger og ved observasjon bidra til sikker padling hos klubbens medlemmer.

Nøkkelvakten er også klubbens ansikt utad ovenfor medlemmer og andre. Nøkkelvaktene aktivt gi råd og veiledning, og skal framstå som serviceinnstilte og vennlige.

## 2. Før oppmøte

Møt opp 15 min før åpningstid – fortrinnsvis med mobiltelefon (viktig ved krisesituasjoner).

Finn fram vaktrapportskjemaet (egen perm) og påse at oppgavene på skjemaet blir utført.

Sjekke vaktperm, under “Beskjeder/ting å gjøre”. Denne siden disponeres av styret og nøkkelvaktgruppe.

Sjekk sikkerhetsutstyr (se liste i klubbhus).

Se hvilke plasser i båtstativene som er tomme, og sjekk dette med utlånslogg.

Ta inn tørre vester.

Mål vanntemperaturen og skriv den på tavla ute på veggen.

Fjern skitt fra brygga med kost og vann.

Sett ut kaffe, te og kakao til medlemmene. Settes ut på benken ved siden av loggpermen.

Hvis annen vakt uteblir og du er alene når klubb skal åpne:

Ring uteblitt vakt.

Søk erstatningsvakt, evt annet medlem med relevant kunnskap.

Gjør en vurdering om det er trygt å åpne klubben. Ventes det f.eks. mye besøk kan det være lurt å vente med å åpne til du får hjelp.

Meld på [vakt@nrpk.no](mailto:vakt@nrpk.no).

Noter fravær i vaktrapport.

### Merknader

Møt opp 15 min før åpningstid – fortrinnsvis med mobiltelefon (viktig ved krisesituasjoner).

Bruk huskelisten

Sjekk sikkerhetsutstyr (se liste i klubbhus).

Sørg for at klubben er tilstrekkelig bemannet før du åpner klubben. 

## 4. Under vaktøkt

Sjekk at alle har gyldig medlemsskap

Klubbens båter kan bare benyttes av medlemmene.

Gi aktiv veiledning om båter, materiell og sikkerhet – ikke bare når du blir spurt.

Ta affære når det ser ut til å bli for mange i en båt eller ved annen feil bruk av utstyr.

Vær OBS på barn alene i båt. Husk 12-årsregelen, men bruk skjønn.

Pass på at alle tar på vest. Det er ikke nok å ha den med i båten.

Sjekk at loggen blir ført av de som låner båt.

Dersom det er flere personer i en båt skal navnene på alle føres i loggen.

Åreboden holdes låst, men gi tilgang til årer for medlemmer som spør.

Se til at klubbanlegget er ryddig; alle vaktene er NRPKs vaktmestre.

Gi beskjed til de som skal på neste vakt.

### Merknader

Sjekk at bare medlemmer bruker klubbens utstyr.

Sørg for at all padling og alt bruk av utstyr skjer på en sikker måte

Bidra til å øke medlemmenes kunnskap og ferdigheter. 

Se til at klubbanlegget er trygt og ryddig.

### Dersom

Det oppstår skade på materiell eller andre spesielle hendelser: noter dette i skadeloggen.

Det er stille/lite besøk: prøv gjerne utstyr du ikke kjenner i området nært klubben.

Det er stor pågang: orienter om makstid på tre timer.

Det er mye folk på brygga: Sett ut skiltet med anmodning om ikke å slå seg til på NRPKs brygge.

Det skjer noe som gir risiko for helse eller miljø: fyll ut «Melding om hendelse» på baksiden av vaktrapport og hvis styret bør varsles øyeblikkelig, bruk kontaktinfo som står på ovenfornevnte skjema.

### Merknader

Hvis nødvendig, rapporter "Melding om hendelse".

Sørg for at dagsturpadlere er tilbake før klubben stenges. Sjekk at anlegget er tomt før det låses.

### Hvis du har tid

Stek gjerne vafler. Vaffelmiks finnes i skapet under benken.

Sjekk dine egne kunnskaper både innen båtmateriell og sikkerhet.

Se på båter og plasseringen av dem. Les informasjon i Vaktpermen.

Ved stengning sjekk om varmeovn er skrudd av før du går hjem.