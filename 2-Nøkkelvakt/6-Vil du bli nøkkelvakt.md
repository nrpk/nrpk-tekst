# Vil du bli nøkkelvakt?

Publisert: 16. september 2017 / Sist oppdatert: 6. april 2018

Alle som får nøkkel til klubbens anlegg må også være villig til å gå vakter i klubbens åpningstid. Som innehaver av nøkkel vil du kunne bruke klubbens utstyr utenom åpningstidene. Utstyret kan kun brukes på Nøklevann.

Styrets nøkkelvaktgruppe vurderer før hver sesong hvor mange nøkkelvakter klubben har behov for. Hvis du er interessert i å bli nøkkelvakt kan du vise din interesse med å skrive til [vakt@nrpk.no](mailto:vakt@nrpk.no) og fortelle oss hvorfor akkurat du vil bli nøkkevakt hos oss.

Aktuelle kandidater går gjennom et kurs og opplæringsvakter før de kan bli nøkkelvakter. Nøkkelvakter skal kunne møte medlemmer på en positiv måte, og kunne gi veiledning og gjøre vurderinger om sikkerhet. Under opplæring må klubben gjøre en vurdering av kandidatenes egnethet til oppgaven. Klubben har et ansvar for sikkerhet, og dette stiller også krav til nøkkelvaktene. Når opplæring er fullført betaler man depositum for nøkkel, signerer avtale og mottar nøkkel før man er nøkkelvakt. Det første året må en nøkkelvakt regne med å ta full kvote nøkkelvakter i tillegg til opplæringsvakter.

**Nye nøkkelvakter må:**

* Ha vært medlem i minst et helt år.
* Være fylt 18 år.
* Ha alminnelig god helse og være godt svømmedyktig.
* En nøkkelvakt må ha e-post, telefon og tilgang til internett – det er våre viktigste kommunikasjonskanaler.

**Hva gjør en nøkkelvakt i Nøklevann ro- og padleklubb?**
Vaktene er klubbens ansikt utad. Vaktene veileder om klubbens utstyr, sikkerhet, klubbens regler og praksis. Det er viktig at alle som er nøkkelvakter er imøtekommende og innstilt på å hjelpe medlemmer som skal ut og padle i klubbens åpningstid. Dette innebærer blant annet å gi informasjon til medlemmer og ikke-medlemmer, være tro mot klubbens bestemmelser og følge de regler og retningslinjer som til enhver tid er gjeldende i Nøklevann ro- og padleklubb.

#### **Opplæring av nye nøkkelvakter**

* Ofte holdes det et informasjonsmøte for de interesserte.
* Kandidaten må før opplæring starte sette seg inn i informasjon under fane «Nøkkelvakt» på klubbens hjemmeside.
* Et nøkkelvaktkurs på 2-3 timer. NB: dette kurset holdes typisk en gang per år – gjerne før sesongåpning!
* Opplæringsvakter – minst 9 timer. Opplæringsvakten skal ta del i oppgavene *før* åpning og *etter* stengning av klubben. Hvis kandidatens opplæringsvakter blir vurdert tilfredsstillende kan vakten få nøkkel.
* Utlevering av nøkkel etter at depositum har blitt innbetalt.
* Etter fullført kurs, inntil 12 timer ordinær nøkkelvakttjeneste den første sesongen.
* For nye nøkkelvakter er nøkkelvaktmøter å regne som obligatoriske; det gjelder de to første vårmøtene og det første høstmøtet etter fullfør opplæring.