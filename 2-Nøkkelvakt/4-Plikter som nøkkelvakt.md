## Plikter som nøkkelvakt

![img](http://nrpk.no/wp-content/uploads/2016/08/fordelerogplikter.png)

Publisert: 9. august 2016 / Sist oppdatert: 27. september 2017

**Generelt**

* Alle som har nøkkel må holde seg oppdatert i de rutinene som er relevant for nøkkelvakter.
* Medlemskontingent og nøkkeldepositum skal være betalt.
* Delta regelmessig på nøkkelvaktmøtene, ett før sesongstart og ett på slutten av sesongen.
* Nøkkel er personlig og kan ikke lånes ut, heller ikke til familiemedlemmer.
* Møte på oppsatte vakter, inntil tre vakter eller 12 timer i løpet av sesongen. Hvis du ikke har mottatt informasjon om dine vakter ved sesongstart må du kontakte [vakt@nrpk.no](mailto:vakt@nrpk.no).
* Skal holde seg oppdatert på, og følge, relevante regler og nøkkelvaktenes HMS-reglement. Dette fins på klubbens hjemmeside.
* Delta på oppfriskningskurs når klubben arrangerer dette.

Alle endringer som er relevante for nøkkelvakttjenesten meldes til vakt@nrpk.no. F.eks. hvis du endrer kontaktinformasjonen eller hvis du melder deg ut av klubben.

**Ansvar for vakt**

Vaktene er helt avgjørende for at klubben kan drive sin aktivitet på en sikker måte. Det er derfor svært viktig at vaktene møter på de vaktene de er satt opp. Når vaktlisten er satt opp har de vaktene som er satt opp på vaktliste ansvar for «sine» vakter. Det vil si at den som er oppført på vaktliste enten må gå vakten selv, eller sørge for at en annen nøkkelvakt tar vakten.

**Ønsker om vakttidspunkt**
Ønsker kan sendes inn til [vakt@nrpk.no](mailto:vakt@nrpk.no) i god tid før vaktliste for sesongen settes opp. Disse blir etterfulgt så langt det lar seg gjøre. Blir man satt opp på vakt på tidspunkt som ikke passer, er den enkelte vakt selv ansvarlig for å finne en annen å bytte med. Etter at vaktbytte er avtalt med annen nøkkelvakt, skal dette meldes inn til [vakt@nrpk.no](mailto:vakt@nrpk.no). Vaktansvarlig sørger for at en oppdatert vaktliste er tilgjengelig på nettsiden og i klubbhytta.

**Bytting av vakt**

Foran i vaktlisten er det beskrevet hvordan du bytter vakt. Hvis du har fått tildelt en vakt som ikke passer er det din oppgave å finne en erstatter. Du må finne en å bytte med ved å (a) spørre andre vakter på telefon, (b) annonsere behov på vaktbytteliste, eller (c) via Facebookgruppe «Vaktbytteønsker NRPK». Hvis du er på Facebook bør du registrere deg i denne gruppen. Når du har avtalt bytte av vakt må du gi beskjed til [vakt@nrpk.no](mailto:vakt@nrpk.no). Hvis du ikke kan møte på vakt må du også gi beskjed til vakttelefonen – så tidlig som mulig.

**Mislighold**

Hvis du ikke møter på vakt må du ta vakten på et senere tidspunkt. Hvis en nøkkelvakt uteblir fra mer enn en vakt vil status som nøkkelvakt bli vurdert.

Ved tap av nøkkel må nytt depositum innbetales før ny nøkkel utleveres. Nøkkel kan kun erstattes en gang. Mister man flere ganger, mister man også retten til å være nøkkelvakt, men man må møte på vakter man er satt opp på i inneværende sesong.

**Inndragning av nøkkel**
En nøkkelvakt kan fratas vervet dersom styret finner at vedkommende ikke er skikket eller egnet til oppgaven.