 Nøkkelvakt

Alle som får nøkkel til klubbens anlegg må også være villig til å gå inn og være vakt i klubbens åpningstid.
Som innehaver av nøkkel vil du kunne bruke klubbens utstyr utenom åpningstidene. Utstyret kan kun brukes på Nøklevann.

Vaktlisten vil blir gjort tilgjengelig for de med registrerte vakter. Lenke til årets vaktliste (passord): [Vaktliste 2018](https://drive.google.com/open?id=1E6YDMWZy_i7TvGM1APVYBhpT7fWwvEAyfxWrmjXm638)

Henvendelser som gjelder nøkkelvakttjenesten ved Nøklevann sendes til [vakt@nrpk.no](mailto:vakt@nrpk.no).