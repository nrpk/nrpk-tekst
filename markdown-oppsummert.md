### Overskrift fra stor størrelse til mindre:

Her fungerer overskriftene som hierarkiske inndelere:	

```markdown
# Overskrift for dokument
## Underdeling
### Minste nivå for inndeling
```

### Typer for utheving av tekst

**Fet tekst**

*uthevelse i kursiv*

```markdown
**Fet tekst**
*uthevelse i kursiv*
```

### Sitater eller annen markant utheving

> uthevet sitat eller tekst du mener er viktig å få frem

```
> uthevet sitat eller tekst du mener er viktig å få frem
```

### Nummererte lister

Merk at rekkefølgen på tallene i nummereringen er likegyldig – det blir alltid presentert riktig.

1. Ditt
1. Datt
1. og helt til slutt

```markdown
1. Ditt 
1. Datt
1. og helt til slutt
```

### Unummererte lister

- Ditt
- Datt
- og helt til slutt

```markdown
- Ditt 
- Datt
- og helt til slutt
```

Lenker og bilder

```markdown
![Markdown logo](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Markdown-mark.svg/128px-Markdown-mark.svg.png)
```

![Markdown logo](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Markdown-mark.svg/128px-Markdown-mark.svg.png)

---

### Mer avansert?

terminologi
:definisjonen av terminologi



```markdown
Terminologi
: definisjonen av terminologi

Fotnoter [^4]
[^4]: Dette er en fotnote
```

