# Kurs

## Introkurs (for å bli medlem i NRPK)

Intro-kurs-påmelding skjer ikke pr. e-post.  Du må melde deg på ventelisten for å få invitasjon til påmelding.  Les hele denne siden, følg linken til «Bli medlem» nedenfor.

NB! Les kvitteringen som kommer opp når du har lagt inn navn og e-postadresse på ventelisten.  Trykk gjerne på linken for å legge ventelisten til din adressebok, for å hindre at senere invitasjoner til påmelding havner i SPAM-filteret.

Det kommer ikke e-post med én gang du melder deg på ventelisten.  Det skjer først når nye kurs settes opp etter hvert og invitasjoner sendes ut til de som på det tidspunktet er på ventelisten.

Vennligst ikke send e-post med spørsmål om når din tur kommer. Det er flere hundre på ventelisten, og når vi sender ut invitasjon til påmelding, blir det vanligvis fort fulltegnet.

> NRPK har stor pågang fra folk som vil bli medlemmer.  Vi har ikke kapasitet til å ta inn alle.  Derfor ble en venteliste opprettet i 2015, for å håndtere tilstrømmingen på en mest mulig rettferdig måte.  Flere hundre hvert år har gjennom ventelisten fått anledning til å ta introkurset som er den obligatoriske inngangen til medlemskap i NRPK.  Det er stadig flere på ventelisten enn kursplasser hver sesong, så det gjelder å være tålmodig.  Når du har meldt deg på ventelisten, må du bare VENTE på en invitasjon til påmelding pr. e-post fra NRPK.

De første syv introkursene i år er fulltegnede.  Flere introkurs blir satt opp på de fleste mandager, fredager og søndager fra uke 24 til ca. midten av juli.  Vi sender ut invitasjoner til påmelding med en prioritering for de som har vært på ventelisten lengst.  Etter hvert går invitasjonene videre til de som har vært på ventelisten i kortere tid.  Rekker du ikke å få meldt deg på et kurs i år, så kommer sjansen igjen neste år.

Slik og bare slik blir du medlem i NRPK:  Introkurs må enhver som er 12 år og eldre ta for å bli medlem i NRPK.  Introkurset er for **alle** som vil bli medlem i NRPK:  Har du tatt kurs i Padleforbundets våttkortstige eller padlekurs andre steder / klubber, må du likevel ta NRPKs introkurs på Nøklevann for å bli medlem i NRPK.  Introkurset koster kr. 300,- i år (2018).

Barn under 12 år må ha følge av foresatte for å bruke klubbens båter og padlebrett på vannet. Derfor slipper de å ta introkurset. De behøver heller ikke å ta introkurset når de siden fyller 12 år, fordi de ved deltagelse i klubben under oppsyn av sine foresatte skal ha lært det nødvendigste.

Les mer om hvordan du blir medlem på siden:  [Bli medlem](http://nrpk.no/om-oss/bli-medlem/)

Der er det også link til venteliste for å ta Introkurs.

## Flattvann

I Padleforbundets våttkortstige er de mest aktuelle kursene:

* Grunnkurs flattvann
* Teknikkurs flattvann
* Aktivitetslederkurs flattvann

Se www.padling.no for en detaljert beskrivelse.

NRPK holder også kurs innen Grunnkurs flattvann til gunstige priser for medlemmene. Dette er et todagers kurs, og annonseres på hjemmesiden. Klubben kan subsidiere teknikkurs og i enkelte tilfeller aktivitetslederkurs – se på hjemmesiden under «Aktiviteter» – «Kurs» – «Subsidiering».

## Hav

Kurstrinnene i Padleforbundets våttkortstige for havpadling er:

* Grunnkurs hav
* Teknikkurs hav
* Aktivitetslederkurs hav

Se www.padling.no for en detaljert beskrivelse.

NRPK holder hver sommer grunnkurs i havpadling til en gunstig pris for medlemmene. Dette er et todagers kurs, og annonseres på hjemmesiden. Klubben kan subsidiere teknikkurs og i enkelte tilfeller aktivitetslederkurs — se på hjemmesiden under «Aktiviteter» – «Kurs» – «Subsidiering».

## Kano

For kano er trinnene på våttkortstigen:

* Grunnkurs kano

* Sikkerhetskurs kano
* Teknikkurs kano
* Aktivitetslederkurs kano

Se www.padling.no for en detaljert beskrivelse.

NRPK setter opp Grunnkurs kano hver sommer på Nøklevann. Dette er et todagers kurs, og annonseres på hjemmesiden. Klubben kan subsidiere sikkerhetskurs, teknikkurs og i enkelte tilfeller aktivitetslederkurs – se på hjemmesiden under «Aktiviteter» – «Kurs» – «Subsidiering».

## Roing

NRPK har foreløpig ikke planlagt noe formelt kurs innen roing i år, men det vil bli gitt instruksjon i roing, mest sannsynlig på enkelte onsdager i løpet av sommeren.
Suvi robåter:

Suvi-båtene er solide, stabile robåter med plass til flere.  Har man ikke rodd før, er disse gode å begynne med.  Det kreves ikke noen spesiell instruksjon for å bruke disse, men spør gjerne en av nøkkelvaktene om hjelp til sjøsetting og igjen når båten skal tas opp av vannet og sveives opp på trallen sin på bryggen.

## Scullere

Før du kan låne våre scullere må du få litt informasjon og veiledning, dersom du ikke har erfaring fra tilsvarende båter. Dette både for å unngå farlige situasjoner (f.eks. velt i kaldt vann) og for å hindre at utstyr blir skadet. Vår Ro-oppmann Jens Chr. kan hjelpe deg. Kontaktes på jcriis @ gmail.com.

## Stand Up Padling (SUP)

Ønsker du også å padle SUP på Nøklevann?
Bli med på et kurs som vi annonserer etterhvert på nettsidene.
Kurset vil holdes på Nøklevann og vil vare ca. 3 timer.
I løpet av kvelden regner vi med at du blir trygg på brettet, lærer padleteknikk, sikkerhetsrutiner og får anledning til å prøve ut de forskjellige brettene vi har.
Du kan begynne på de mest stabile, brede brettene. Kanskje du får lyst til å prøve sprint-brettet allerede på kurset!

Vi legger vekt på at du skal bli trygg på brettet og også øve på av- og påstigning, både ved brygga og andre steder.

Kurset er forbeholdt dere som allerede er medlem i NRPK.

Tid: Kanskje en onsdag i juni, kl.18.30-21.30
Pris: kr. 100,-
Instruktører: Erfarne NRPK-folk
Påmelding: Benytt NRPKs hjemmeside når vi har satt opp en dato.

Nærmere info kommer etter hvert.

## Livredningskurs

NRPK arrangerer hvert år livredningskurs med instruktør fra Norges Livredningsselskap. Kurset varer ca 2,5 timer og holdes på Holmlia bad, hvor klubben også har sine bassengtreninger med kajakk.
Kurset gir et kompetansebevis i livredning som er gyldig ut neste kalenderår. Kursdeltagere forplikter seg til å være til stede som bassengvakta bad to ganger. Dette er en forutsetning for at klubben kan leie basseng av Oslo kommune.

Kurset inkluderer redningsteknikker i basseng og grunnleggende hjerte- lungeredning. Kurset inkluderer 200 meter svømming, derav 100 meter rygg, og å øve på å dykke etter dukke. Avslutningsprøve består i å hente opp en dukke på bunnen og svømme med en person en bassenglengde.

Kurset er åpent for alle medlemmer i klubben, men nøkkelvakter vil bli prioritert hvis det blir fulltegnet. Vi oppfordrer nøkkelvaktene til å ta kurset. Kurset er gratis.

## Subsidiering

NRPK dekker hele kursavgiften når medlemmer tar et eller flere av følgende kurs:

* Førstehjelpskurs (hjerte-/lungeredning)
* Livredningskurs
* Aktivitetslederkurs

Den som får dekket aktivitetslederkurs vil måtte kunne holde grunnkurs som gjenytelse til klubben.

Deltagelse på slike kurs skal forhånsdsgodkjennes av styret.  (epost:  styret @ nrpk.no)

NRPK arrangerer jevnlig kurs etter etterspørsel, der det gis rabatt for medlemmer:

* Grunnkurs kano/Sikkerhetskurs kano
* Grunnkurs roing
* Grunnkurs hav
* Grunnkurs flattvann
* Teknikk-kurs hav
* Teknikk-kurs flattvann
* SUP-kurs

For medlemmer som tar eksterne våttkortkurs som ikke tilbys av NRPK, for eksempel grunnkurs elv, kan NRPK eventuelt dekke en del av kursavgiften.  Dette må forhåndsgodkjennes av styret og eventuelt kurstilskudd utbetales etterskuddsvis mot oversendelse av kvittering.