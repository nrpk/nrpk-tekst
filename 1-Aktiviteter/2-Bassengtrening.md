# Bassengtrening

Bassengtreningene er åpne for alle som ønsker å gjøre øvelser i kajakk for å bli mer trygg i vannet, uansett ferdighetsnivå. Ingen forhåndskunnskap er nødvendig, men du må være medlem i NRPK. Bassengtreningen er gratis. Øvelser på klubbens “ordinære” bassengtreninger kan f.eks. være selvberging, støttetak, eller rett og slett å velte under kontrollerte forhold. Noen ganger holdes det kurs eller det blir fokusert på spesielle temaer. Informasjon om dette gis på klubbens hjemmeside.

## For nybegynnere:

* Utvikle følelse med kajakk og vann
* Prøve å velte under trygge forhold – «Våt utgang» uten spruttrekk
* Våt utgang med spruttrekk – Avtal hjelp!

## For viderekommende kan vi foreslå ytterligere ferdigheter:

* Lavt og høyt støttetak.
* Cowboy-entering, evt. entring med årepose.
* Kameratredning – Kamerat støtter kajakk ved entring.
* Kameratredning – den som har veltet heiser seg opp etter baug til hjelperens båt.
* Tømming av veltet kajakk, gjerne over annen kajakk.
* Våt utgang med spruttrekk, med innlagte vanskeligheter. For eksempel stramt spruttrekk, manglende utløserstropp, padlehandsker, med en hånd (aktuelt hvis f.eks. skulder er ute av ledd), eller gjøre det i blinde.
* Det er mulig å øve på eskimorulle også. Eskimorulle lærer man raskere hvis man melder seg på et kurs (vi har dessverre ikke det). Det vil hjelpe å se en film om emnet, for eksempel «The kayak roll» av Kent Ford.

Selv om de ordinære bassengtreningene ikke arrangeres som kurs vil vi kunne tilby hjelp og tips til de som ønsker det.

## Praktisk informasjon

Bassengtreningene holdes på Holmlia bad. Klubben stiller med utstyr som kajakker, årer og åreposer. Du må ta med det du pleier å ta med når du er i badebasseng, som:

*   Badetøy (det skal ikke loe)
*   Håndkle
*   Såpe / Shampo
*   En 10-er til garderobeskapet

Kanskje også:

*   Vannflaske – tilgang til vann er ikke den beste
*   Dykkemaske (evt. svømmebriller + neseklype)

Vi får disponere lagringsplass på Holmlia bad, men det vil noen ganger likevel kunne være behov for transport av båter og utstyr i forbindelse med bassengtrening. Det vil i så fall bli gitt beskjed om dette.

 

## Holmlia bad 28. februar 2015

### Sted for oppmøte:

Vi går inn inngang SØR til Holmlia bad / Actic treningssenter, rett under Holmlia skole (Nordåsveien 3):
https://goo.gl/mzZBbo

Hovedinngangen til Holmlia bad, som vender mot Holmlia senter, er normalt stengt når vi har bassengtrening.

### Når du kommer med tog / buss:

Ikke bruk inngangen mot Holmlia senter, men gå rundt til inngang sør (se link). Det er 750 meter å gå og tar ca 10 minutter. Regn litt ekstra tid første gangen for å finne fram.
https://goo.gl/i7mhHb

### Når du kommer med bil:

Parker her: https://goo.gl/mvwjCs

Deltagere skal ha gjort seg kjent med regler for bassengtreningen – se under “Aktiteter / Bassengtrening”.

## Sikkerhetsregler

Hensikten med bassengtrening er å gi deltagerne anledning til, gjennom «lek» og øvelser, å lære seg sikkerhetsfremmende teknikker under trygge forhold. 

Regler for bassenget:

Bassengvaktene har hovedansvar for sikkerhet i og ved bassenget, og deltagerne må respektere pålegg fra vaktene.
Alle over 12 års alder som deltar på bassengtrening skal ha gjort seg kjent med sikkerhetsreglene. Barn under 12 år må komme i følge med voksne, som også har ansvar for oppsyn med disse.
Alle skal ha et øye med de andre i bassenget for å se om noen får problemer. Kajakker som forlates i vannet skal være snudd «riktig» vei for å gi bedre oversikt.
For øvelser med spruttrekk gjelder følgende: Før padleren har vist at han behersker våt utgang med spruttrekk skal dette øves på grunt vann med en person stående ved siden av båten.
For uøvde padlere skal eskimorulleøvelser og velt med spruttrekk skal skje sammen med en annen person. Dette skal være avtalt slik at den andre er klar til å assistere.
Hvis du henger opp-ned og trenger hjelp – slå to ganger hardt på skroget for å få oppmerksomhet.
Vær forsiktig og se hva som er rundt deg for å unngå skade på folk, materiell og anlegg.
Vi ber de som forlater bassenget for kortere eller lengre tid å gi beskjed til en bassengvakt slik at vi kan holde oversikt.
Ut over dette følger vi de alminnelige reglene for Oslobadene i den grad det passer for vår type aktivitet:

http://www.bymiljoetaten.oslo.kommune.no/idrett_og_fritid/svommehaller_og_bad/baderegler/


## For bassengvakter

Bassengvaktene har hovedansvar for sikkerhet, og skal sørge for nødvendig tilsyn ved bading, bistå med hjelp når nødvendig, og bistå ved evakuering av bygning ved evt brannalarm.
Det skal alltid være minst én bassengvakt med livredningskurs til stede når det er aktivitet i vannet.
Bassengvaktene skal ha avtalt seg i mellom hvordan de skal organisere beredskapen. Hvis det er flere uøvde eller unge deltagere kan det være hensiktsmessig å ha en bassengvakt på land med fullt fokus det som skjer i vannet. Ofte vil det være tilstrekkelig at en bassengvakt er i vannet.
En gang hvert halvår skal alle bassengvakter gå i gjennom plassering av rømningsveier og sikkerhetsutstyr (førstehjelpsutstyr, brannslukkingsutstyr, teppe etc). Bassengvaktene gjør dette på eget initiativ – fortrinnsvis ved start av høst- og vårsesong.