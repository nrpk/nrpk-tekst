# Roing

Et unikt tilbud til alle medlemmer i NRPK om gratis instruksjon i roing i sommer! Meld din interesse til Jens Christian Riis. 

Jens Christian Riis er Ro-oppmann ved NRPK og en meget dyktig roer. Han begynte å ro når han var 7 år gammel og har i voksen alder vært aktiv mosjonsroer og «masters»-roer. Han tilbyr nå instruksjon i roing for klubbens medlemmer etter avtale, torsdagene 24. og 31. mai og så på de fleste onsdager i juni og første halvdel av juli, kl. 18.00 – 21.00.

De som er interessert må gjerne sende en e-post til jcriis@gmail.com eller ring ham på tlf. 917 47 975.  Det er kun plass til 4-5 personer samtidig.