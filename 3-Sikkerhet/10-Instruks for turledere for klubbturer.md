# Instruks for turledere for klubbturer

## **Omfang**

 

Instruksen gjelder den som leder eller deltar i organisering av klubbturer i Nøklevann Ro- og Padleklubb. En klubbtur skal være godkjent av NRPKs styre, og skal være annonsert som klubbtur på klubbens hjemmesider.

 

## **Tidlige forberedelser og annonsering**

 

* En klubbtur må være godkjent av styret på grunnlag av en presentert turplan, eventuelt med alternativ rute etter behov.
* Turleder og turassistenter godkjennes av styret. En gruppe på 10 turdeltakere skal inkludere turleder med minst én assistent. Turlederen skal pekes ut samtidig med at styret godkjenner turen. Assistenter skal fortrinnsvis godkjennes samtidig, men kan også pekes ut på et senere tidspunkt i god tid før turdato.
* Turen skal annonseres i så god tid som praktisk mulig i gjennom klubbens kanaler (hjemmeside).
* Ved annonsering av tur skal turens vanskelighetsgrad beskrives. Det skal stilles krav til deltagernes ferdigheter og om turen er egnet for barn.
* Ved annonsering skal det gå frem at alkohol og andre rusmidler ikke er tillatt.

## 

## **Før turen begynner**

 

* Hvis turleder ikke kjenner farvannet bør ruten prøvepadles sammen med assistentene.
* I rimelig tid før avreise skal det finnes en plan som omfatter kjørerute, møteplass med mere.
* Før avreise skal det eksistere en plan med hvilepauser, landingssteder, overnattingsplass osv. Lag kartskisser med inntegnet rute til alle deltakere, samt en kort turbeskrivelse med planagte aktiviteter og tidspunkter.
* Send turbeskrivelse og utstyrsliste til deltakerne, gjerne en uke før avreise. Legg ved en oppfordring til å ikke etterlate spor i naturen.
* Ved leie av båter må dette på forhånd avtales med utleier.
* Eventuell tillatelse av grunneier må innhentes.
* Risikofaktorer for turen må vurderes av turleder sammen med assistenter. Forhold som må vuderes er deltakernes ferdigheter, kryssing av vann (avstand til land), stryk, vanntemperatur, isforhold, vær med mere. Vurder om det bør være en alternativ plan hvis vær skulle gjøre det nødvendig.
* Husk bålforbud i perioden 15 april til 15 september. Det kan finnes godkjente bålplasser, eventuelt kan det lokale brannvesen kontaktes.
* Det skal lages en deltagerliste med eventuelle mobiltelefonnumre før turen.
* Bilutgiftene avtales før avreise i mellom de som deler bil. Et forslag kan være å bruke statens satser, og dele dette på antall personer.

## 

## **Om utstyr**

 

* Turleder og turassistent har med kasteline eller tau for å kunne gi assistanse.
* Førstehjelpssett skal være med, og innholdet kontrollert.
* Øsekar eller pumpe, samt reserveåre skal være med.
* Poser / sekker for søppel, samt lerretsteip for å takle praktiske problemer.

Blant våre anbefalinger om turutstyr:

* Flere lag med hurtigtørkende klær som ull, polyester («superundertøy») eller fleece. Stillongs og undertøy kommer utrolig ofte i bruk også midt på sommeren! Vindklær / regnklær.
* Tresesong sovepose, eller sommerpose med «tillegg», til sommerturer når det er plussgrader.
* Klær og sovepose pakkes i vanntette sekker. Vanntette pakksekker eller søppelsekker (dobbel) kan brukes.

## 

## **Ved oppstart**

* Turleder tar avgjørelse i henhold til vær og værvarsel.
* På oppmøte-/startplass: Kontroller at alle deltakere har med seg det nødvendige utstyret, og at deres kvalifikasjoner svarer til kravene som turen stiller.
* Kort briefing om ruten (utdeling av kart).
* Avtale padleformasjon: For eksempel kan turleder lede gruppen, og turassisten kan være siste padler. Gruppen må holde sammen. Ingen skal etterlates, ingen skal forlate gruppen!
* Briefing om hva som skal gjøres i tilfelle uhell (velt).
* Informer om at vi praktiserer «meld fra hvor du går (eller padler)» når vi har slått leir. Det skal også informeres om at vi holder en tilsvarende gjennomgang før vi padler tilbake fra leiern.
* Turleder tar en siste kontroll når alle er på vannet, før formasjonen opprettes.

## **Underveis**

 

Tempoet skal tilpasses den svakeste deltaker. Ingen må bli for slitne.

Formasjonen skal ikke blir for langstrakt.
Hvis turen blir slitsom for noen kan alternativer som ekstra pauser, bytte parkamrater, bytte båter, eller kortere/lettere rute vurderes.
Ta en sjekk når vi forlater rasteplasser for å sikre at ting eller avfall ikke er glemt.
Ved alvorlig hendelser varsles styrets leder, evt HMS-asvarlig.

 

**Avslutning**

* Tilbakelevering av leid utstyr.
* Be om evaluering av turen hvis ønskelig.
* Førstehjelpsutstyr gjøres i stand til neste gangs bruk.
* Turleder rapporterer til styret så snart som mulig etter turen, for eksempel per E-post. Var turen passe krevende for deltakerne, hvordan var risiko underveis, tilbakemeldinger fra deltakerne, bør turen gjentas – og i så fall med hvilke endringer?

