# Sikkerhet og bruk av Holmlia bad

Publisert: 14. august 2016 / Sist oppdatert: 25. august 2016
for bassengvakter

Leieavtale

Det står i leieavtale at “Med leietiden forstås all tid i anlegget, inkludert bruk av garderober før og etter bruk av bassenget.”. Ansatte ved Holmlia bad sier derimot at leie gjelder bruk av basseng. (Denne tolkingen gir leietakere mer tid i bassenget.)

Sikkerhet

Alle deltagere skal kjenne “Sikkerhetsregler for bassengtrening” som fins på klubbens hjemmeside. Link til denne skal sendes til alle deltagere av bassengtrening før de møter.

Hvis antall deltagere er flere enn seks i tillegg til bassengvakt, skal bassengvakten rådføres.

 

Generelt ved opphold i anlegget
 

Ansvarlig for bassengtrening skal holde orden på antall deltagere under bassengtrening. Ansvarlig har nøkkel til anlegget. Nøkkel plasseres på avtalt samlingspunkt for evakuering.

Deltagere i bassengtrening skal holde seg samlet mens de er i det underjordiske anlegget. Hvis en deltager må forlate gruppe gjøres det etter avtale med bassengvakter.

Bassengvakter sørger for at det er en telefon tilgjengelig til enhver tid, for eksempel skal telefon være tilgjengelig ved bassenget.

 

Evakuering
 

Siden anlegget er underjordisk er kravet til organisert evakuering viktig. Evakueringsrutine:

I tilfelle alarm, eller ved behov for evakuering, samles vi ved samlingspunkt.

Samlingspunkt er ved bassenget, like ved gangen inn til garderobene.

Når alle er samlet (tell!) forlater vi bassenget gjennom garderobene (for å komme på rett side av branndør). Hvis vi deles i gjennom garderobe (dame / herre) teller vi på nytt utenfor garderobene.

Vi går ut i samlet tropp og går bort til parkeringsplassen. Når vi er ute forlater ingen gruppen før det er avtalt med ansvarlig.

Båter og utstyr

Årer, padlevester, spruttrekk og åreposer låses med hengelås / wire.

Båter oppbevares ulåst.

Ved skader på utstyr varsles bassengansvarlig, fortrinnsvis via basseng@nrpk.no.

Låsing og alarm

Vi går alltid inn og ut i gjennom utgang mot sør-vest, mot Holmlia skole.

Når vi går inn lar vi ytre dør og rulleport stå i samme posisjon som når vi kom. Lys i korridoren kan stå på mens vi er inne.

OBS – deaktiver alarm hvis nødvendig! (Bruk nøkkel!)

Hvis vi er de siste som forlater anlegget – dvs det er ingen i hallen på den andre siden av gangen – slår vi av lys i gangen, aktiverer alarm, og låser ytre dør / senker rulleport.

Leietakers plikter

I avtale med Oslo kommune står blant annet:

Det påhviler leietaker å utføre lett overflatisk renhold. Dette innebærer å:

Stenge dusjer

Plukke opp søppel i alle benyttede områder, inkludert garderober

Fjerne hår fra sluk, samt spyle dusjer ved behov

Rydde på plass alt utstyr etter bruk

Fjerne gjenglemte ting.