# Sikkerhetsutstyr på klubben

Publisert: 14. august 2016 / Sist oppdatert: 25. august 2016

I klubbhytta har vi følgende sikkerhetsutstyr:

* Førstehjelpskoffert
* Munn-til-munn maske (også i fjørstehjelpskoffert)
* Nakkekrage
* Kasteline
* Taueline med belte
* Brannslukkingsapparat (pulver)
* Brannteppe
* Badebleier, 2 størrelser
* Dykkermaske
* Vannkikkert
* Undervannslykt
* Hodelykt
* Ulltepper, 2 stk
* Ekstra batteri til lykter

Sikkerhetsutstyr som er utendørs:

* Redningsflåte med lang åre
* Båre
* Livbøyer, 2 stk (utenfor og innenfor klubb)
* Pulverapparat (kajakkstativ)

Hvis noe mangler, gi beskjed til materialforvalter. Denne listen vedlikeholdes av Materialforvalter sammen med HMS-ansvarlig.