# Sikkerhet ved padling

**Av Knut Sørby**

*(Artikkelen ble skrevet i 2007, oppdatert aug 2015)*

En stille og varm sommerdag på Nøklevann er tanken på at det kan skje ulykker kanskje ikke nærliggende, men padling er en potensielt farlig aktivitet. I NRPK har vi heldigvis vært forskånet for tragiske ulykker, og vi håper denne positive situasjonen vil vedvare. I klubben vår er det nå over 3000 medlemmer, og det er nok ikke veldig galt å si at mange er nybegynnere når det gjelder båtliv og sikkerhet. Styret har derfor iverksattt en del tiltak for å bidra til at vi også i fremtiden skal kunne bedrive ro- og padleaktivitet uten ulykker. Et av bidragene er denne artikkelen, og klubben arrangerer også forskjellige kurs, der sikkerhet vil være et av hovedtemaene. Nøkkelvaktkurset er spesielt myntet på nøkkelinnehaverne som står vakt på anlegget i åpningstidene. Fra sesongen 2008 ble det obligatorisk at alle nye som ønsker å bli medlem først må gjennomgå et 3-timers introduksjonskurs. Dette har vært et stort løft for klubben.

 

 

VIKTIG: De som ønsker å padle på fjorden eller langs kysten bør absolutt skaffe seg både mer kunnskap og en del utstyr. Norges padleforbund har utarbeidet en kursstige for havpadling, elvepadling osv. Disse kursene leder frem til et slags sertifikat som kalles Våttkort. Mer info på www.padling.no

## A) Bruk av vest

I vår klubb er regelen klar: **det er påbudt å bruke vest**. Barn er som regel flinkest til å bruke vest, men hvordan skal du som voksen (og kanskje en dårlig svømmer) kunne ta deg av barna ved en kantring når du har mer enn nok med å ta vare på deg selv? Det er ikke lett å få på seg vest når du først ligger i vannet.  Derfor: Alle som bruker klubbens båter skal ha vest på – det er ikke nok å ha den med i båten. Små barn og de som ikke kan svømme må ha redningsvest.. Husk at vester må sitte godt, dvs ikke være for store.

 

 

I klubben vår har vi to hovedtyper flyteutstyr; flytevest og redningsvest. Inndelingen går på kravene til oppdrift. Dette måles i newton (N). Flytevestene (også kalt padlevester) er merket 50 N. Disse finnes i en rekke ulike former og farger, og er beregnet på å gi personen en loddrett flytestilling. Flytevester er beregnet på svømmedyktige personer over 30 kg.

 

 

Den tradisjonelle redningsvesten er merket 100 N. Den er beregnet på både svømmedyktige og ikke-svømmedyktige personer. I utgangspunktet skal kragen og flyteelementene foran på vesten bidra til å snu en bevisstløs person over på ryggen og gi en flytestilling på ryggen med ansiktet over vann. Du kan imidlertid ikke være sikker på at det skjer. Barn som bruker for stor vest kan falle ut av vesten eller flyte med ansiktet ned i vannet.
Barn med vanlig bleie kan bli liggende med nese og munn under vann fordi den vanlige bleien har så stor oppdrift at redningsvesten ikke klarer å rette opp barnet. Skift derfor ut bleien med en egen badebleie før du tar med deg bleiebarn ut i båt, eller la dem sitte i båten uten bleie. Badebleier finnes i klubbhuset.

## B) Ombord- og ilandstigning

Ved ombordstigning i en kajakk eller kano må du alltid sørge for å holde tyngdepunktet rett over senterlinjen. Hold deg i bryggekanten eller få en kamerat til å støtte båten. I en kajakk går du om bord ved å holde midt på cockpitkanten foran med en hånd og i bryggekanten med den andre. Plasser så føttene midt i båten mens du holder tyngdepunktet over båtens senterlinje. Sett deg rolig ned på setet og strekk så ut beina mens du fortsatt holder i bryggekanten.

![img](http://nrpk.no/ecas-uc-2016/wp-content/uploads/2016/08/sp2-300x256.jpg) ![img](http://nrpk.no/ecas-uc-2016/wp-content/uploads/2016/08/sp1.jpg)
En mer sikker metode er å sette seg ned på brygga ved siden av cockpiten. Du holder en hånd i bryggekanten og en hånd midt bak på cockpitkanten. Så stikker du bena ned i kajakken og løfter deg rett ned på setet. Når du skal ut av kajakken gjør du det i motsatt rekkefølge. Fordelen med denne metoden er at tyngde-punktet hele tiden er veldig lavt, slik at risikoen for å velte er minimal.

![img](http://nrpk.no/ecas-uc-2016/wp-content/uploads/2016/08/sp3.jpg)
Skal du gå ut fra en strand eller lignende, må du passe på at et eventuelt ror er klar av bunnen.

## C) Kantring

Dersom du velter, svøm til land. IKKE bruk opp kreftene på å få med deg båten. For å unngå lange svømmeturer i kaldt vann bør du padle mest mulig langs land. Da opplever du også mer av naturen. Husk bare å holde avstand til de som bader.

 

 

Spesielt interesserte kan lære seg redningsteknikker og eskimorulle. Redningsteknikker kan læres på kurs som klubben arrangerer.

![img](http://nrpk.no/ecas-uc-2016/wp-content/uploads/2016/08/sp4-300x145.jpg)

Eskimorulle anses egentlig ikke som en redningsteknikk, men for de som klarer den, er det både gøy og nyttig.

## D)Tømming av kano og kajakk

Etter en velt er det viktig å få tømt båten for vann. Med litt trening kan dette gjøres ute på vannet, men det er enklere hvis du kan stå inne på grunna. Du må aldri prøve å dra en vannfylt båt opp på land eller på brygga! På grunn av vekten av vannet kan båten da knekke. En kano snur du med bunnen opp for å få tømt den. Du løfter baugen opp for så raskt å snu kanoen på rett kjøl.

 

 

En vannfylt kajakk legges med bunnen ned. Du trykker fronten ned slik at vannet renner frem i kajakken. Deretter løfter du fronten opp samtidig som du snur båten med bunnen opp. Du må være rask og løfte så høyt at cockpiten kommer klar av vannet. Vannet vil da renne ut av cockpiten. Gjenta dette noen ganger. Er kajakken helt full av vann, må du først ta tak på siden av cockpiten og løfte svært langsomt slik at en del av vannet renner ut. Deretter kan du følge beskrivelsen ovenfor.

 

 

Et godt råd er at du alltid har med deg håndkle og et tøyskift når du er på sjøen. Dette bør være pakket i en vanntett sekk. Slike får du kjøpt i mange størrelser, og de koster ikke mye. Tips: finn noen gamle klær, f eks joggedrakt, genser, lue, sokker, vindtett jakke og et håndkle. Pakk dette i en vanntett sekk og ha den alltid med når du er ute og padler.  Det kan også være  lurt å putte mobiltelefon,  lommebok, bilnøkler osv i en liten vanntett pose.

## E) Bruk av spruttrekk på kajakk

For uøvde kan dette være risikabelt, og vi anbefaler sterkt at trekk bare benyttes av de som har øvd på redningsteknikker. Faremomentet er at man ved kantring kan bli hengende fast under båten. Dødsulykker har skjedd. Faren oppstår dersom du ikke vet hva du skal gjøre ved en velt. Det aller beste er selvfølgelig at du behersker eskimorullen. Kan du ikke den, er alternativet forsåvidt enkelt: du skal trekke i håndtaket/sikkerhetshempen foran på trekket for å løsne det, og deretter skyve deg ut av kajakken.

 

 

Dersom håndtaket på trekket har kommet på undersiden når du tok det på, har du et alvorlig problem. Når du henger opp ned vil din egen vekt kunne forårsake at trekket sitter meget stramt rundt cockpiten, og det vil være nesten umulig å løsne det.

![sp5](http://nrpk.no/ecas-uc-2016/wp-content/uploads/2016/08/sp5.jpg)

Skal du bruke trekk på havkajakk eller elvekajakk, sørg for at du øver på å komme deg ut av kajakken under vann. Dette må øves under betryggende forhold, det vil blant annet si at du har en hjelper tilstede. Hjelperen bør stå på bunnen ved siden av båten når dette øves.

 

 

For padling på Nøklevann holder det fint å bruke halvtrekk. Dette beskytter mot vannsprut og er ufarlig å bruke.

## F) Vannets farer – drukningsmåter

De som omkommer i vann kan vi inndele i fire grupper etter måten de omkommer på. Tre av dem kalles «å drukne» og den fjerde «å fryse i hjel».

 

 

**Vann i lungene**
En vanlig måte å drukne på er at lungene blir fylt med vann slik at man synker. Hver gang man puster inn under vann, får man en snau halv liter vann ned i lungene. Fordi lungene fylles litt etter litt, vil en som drukner på denne måten, vanligvis komme opp til overflaten flere ganger før personen synker til bunns for godt. Hver gang den som er i ferd med å drukne har hodet over vann, har personen nok luft til å kunne rope om hjelp.

 

 

Når vannet er kaldt, personen litt påvirket av alkohol og lufttemperaturen høy, har det skjedd at personer har hoppet ut i vannet og aldri mer kommet opp for å puste og rope om hjelp. Når varm hud utsettes for kaldt vann, oppstår en sterk trang til å puste raskere (denne refleksen kan man merke under en kald dusj), og vedkommende trekker inn vann flere ganger allerede første gangen under vannet.

 

 

**Krampe i stemmebåndene**
Den andre gruppen av druknede er personer som har fått vann bak i halsen og ned på stemmebåndene. Det har ført til krampe i båndene, og personene er blitt kvalt uten at vann har trengt ned i lungene. Personer i denne gruppen dør uten å rope om hjelp og uten å synke til bunns.
Når livløshet inntreffer, slutter pustebevegelsene og krampen i stemmebåndene. Vann trenger bare meget langsomt inn i lungene, og den forulykkede blir flytende i vannoverflaten.

 

 

**Kuldeallergi**
Den tredje gruppen av druknede er personer som er allergiske eller overømfintlige mot kulde. Allergien er som regel en medfødt lidelse som vedkommende kjenner til. De prøver å unngå å komme ut i kaldt vann, og de kler seg riktig i kjølig vær.

 

 

Noen personer kan imidlertid få allergi mot kulde etter andre sykdommer, særlig etter virusinfeksjoner i lungene. Slike tilfeller kan få tragiske utfall dersom vedkommende ikke kommer på land straks det merkes pustevanskeligheter.

 

 

**Å fryse i hjel**
I vår tid er det blitt vanlig å skjelne mellom de tre typiske drukningsmåter (pkt 1-3), som kan kalles akutte drukninger, og tilfellene der forulykkede langsomt fryser i hjel på grunn av et lengre opphold i vann. Det skjer oftere at personer fryser i hjel enn at de omkommer ved akutt drukning.

## G) Nedkjøling (hypotermi)

Kroppen er ømfintlig for endringer i kroppstemperaturen. Synker kroppstemperaturen med 2-3 grader, er du som regel ikke i stand til å ta vare på deg selv. Hvis kroppstemperaturen synker ned mot 30 grader, er det alvorlig fare for livet. Med våte klær på kroppen kan man faktisk fryse i hjel midt på sommeren hvis det blåser kraftig.

## H) Hvor langt kan du svømme?

Distansen avhenger først og fremst av vanntemperaturen. I Norge regnes alt utendørs vann for å være kaldt vann, selv om sommeren. Ikke overvurder muligheten for å svømme til land etter en velt. Med vanntemperatur på +10 til 15 grader vil en person normalt ikke klare å svømme mer enn et par hundre meter. I vann med temperatur ned mot 0 grader vil en person, uavhengig av hvor god han er til å svømme, ikke klare mer enn 25-50 meter. Disse tallene er gjennomsnittlige, og vil variere med en persons tilstand og påkledning.

———————————————————————————————————————————————————————————————————————-

Bakgrunn for vestpåbudet i NRPK (som klubben innførte i 2008)
Artikkelen nedenfor er skrevet av Knut Sørby etter omfattende studier av forskningsrapporter, statistikk osv som gjelder sikkerhet på vannet, drukning, hypotermi mm)

 

 

Bruk av vest er det absolutt viktigste sikkerhetstiltaket når man er i båt. Ved en kantring kan spørsmålet om vest være helt avgjørende – faktisk et spørsmål om liv eller død. Dette gjelder spesielt i kaldt vann. Den offisielle amerikanske definisjonen av kaldt vann i denne sammenheng er vann med temperatur under 20 grader!

 

 

All statistikk viser at bruk av flyteplagg bidrar betydelig til sikkerheten. 70 prosent av dem som omkommer i fritidsbåtulykker har ikke benyttet flyteutstyr. For USA og Canada er tallet 90 prosent. De aller fleste drukningsulykker kunne vært unngått ved bruk av vest. De fleste drukningsulykker skjer mindre enn 10 meter fra land, brygge eller annet sikkert sted. Ca 40 prosent av drukningene skjer under 2 meter fra sikkerheten.

 

 

Med vest slipper du å bruke krefter på å holde hodet over vann. Vesten gjør at du kan konsentrere deg om selvredning og eventuelt om å hjelpe andre.
Kuldesjokket man får når man faller ut i kaldt vann antas å være hovedårsaken til de aller fleste drukningsulykker i vann med temperatur under 15 grader. Den første reaksjonen i møte med kaldt vann er at man ufrivillig trekker pusten dypt. Skjer dette før man får hodet over vann er man umiddelbart i en livsfarlig situasjon.

 

 

Hyperventilering (rask pusting) er et alvorlig problem rett etter at man har falt ut i kaldt vann. Statistisk har man bare et minutt på seg til å få pusten under kontroll.
Uten vest vil du etter få minutter i kaldt vann ikke klare å svømme effektivt, men bare kjempe for å holde hodet over vann. Statistikk viser at man i beste fall bare klarer 10 minutter med fornuftig bevegelse, dvs svømming eller redning (se videolink nederst).

 

 

Det er slitsomt og nesten håpløst å få vesten på seg etter at du har falt i vannet. I kaldt vann vil fingrene i løpet av få minutter bli så kalde at du ikke klarer å få på deg vesten skikkelig.

 

 

Noen tror at små vann som Nøklevann er ufarlige, men norsk statistikk forteller at en tredjedel av drukningsulykkene skjer i ferskvann. I USA skjer 60 prosent av drukningsulykkene på innsjøer, 25 prosent på elver og 15 prosent på havet.

 

 

Klubbforsikringen fra if, som dekker alle våre medlemmer, gjelder bare når du har vesten på.

 

 

Ifølge våre medlemsregler skal  du ha vesten på, det er ikke nok å ha den med i båten. Medlemskap i NRPK forutsetter at du følger medlemsreglene.

 

 

**”Å ha vest tilgjengelig i båten er det samme som å satse på at du kan ta på deg sikkerhetsbeltet rett før bilen din kolliderer” (talsmann for US Coast Guard).**

 

 

**Vesten virker ikke hvis du ikke har den på…..**

 

 

Kilder:
Sjøfartsdirektoratet
US Coast Guard
American Canoe Association
National Transportation Safety Board (USA)
Canadian Safe Boating Council
www.coldwaterbootcampusa.org (fakta og videoklipp – Anbefales!)
https://vimeo.com/4534662 (28 min video fra Cold Water Boot Camp – Anbefales!)



