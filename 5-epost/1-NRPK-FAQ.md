> NRPK Ofte Stilte Spørsmål
> NRPK Frequently Asked Questions

Merknad: ==Her er det sikkert masse feil og misforståelser, utydelig språk og andre uklarheter. Grunnen til at jeg organiserer dette er for at vi skal stå stødig i møte med spørsmål fra kommende og eksisterende medlemmer.== 

###### Uthevet linje (som overskrift) er en generalisering av et spørsmål vi ofte mottar.

Første avsnitt prøver å beskrive et generelt svar til spørsmålet og skal være fullstendig og endelig. Er det flere spørsmål i teksten, bruker vi et avsnitt pr spørsmål, ikke fler. Alle svar må få plass i ett avsnitt, hvor vi generellt prøver begrense forklaringen på spørsmålet under 4 setninger. 

*Siste avsnitt skrives i kursiv, og forteller om hvor en kan finne nærmere opplysninger og forklaringer på hvorfor ting er som de er.*

###### Kan jeg leie hos dere for en dagstur på lørdag?

Vi leier bare ut til medlemmer. 

*Se mer ...*

###### Har meldt meg på introkurs men har ikke fått noen tilbakemelding. Hva gjør jeg?

Når du melder deg på kurs, melder du deg egentlig på en mailing-liste som har som eneste oppgave å fortelle når kursene blir tilgjengelige. Alle som melder seg på denne mailinglisten vil få en mail så snart som kurset blir organisert, hvor du kan bekrefte at du vil melde deg på. Det er første mann til mølla som gjelder, så når det er stor pågang (hvilket det ofte er), vil kursplassene forsvinne fort dersom du venter for lenge med å bekrefte plassen din. Lurt å på forhånd sjekke at epost-klienten din at den ikke markerer epost fra NRPK som søppelpost.

*Se mer om hvordan du kan sjekke epost-klienten din ...*

###### Jeg er allerede medlem, men har flere i familien (< 12 år) som ikke er medlemmer men som ønsker å bli med. Hva gjør jeg?

Svar

*Se mer*

###### Jeg er allerede medlem, men har flere i familien (>= 12 år) som ikke er medlemmer men som ønsker å bli med. Hva gjør jeg?

Svar

*Se mer*

###### Vi er et lag som ønsker å ha tilrettelagt aktivitet for personer med `xxx`. Hva gjør vi?

Vi driver ikke med utlån da vi er et idrettslag. Men vi har instruktører med kompetanse til å tilrettelegger padling, for personer med `xxx`. Styret i klubben kan kontaktes direkte på telefon `zzz` eller `yyy` for å svare på din hendvendelse.

*Se mer*

###### Jeg og mine barn har vært medlem i flere år. Nå er mitt barn over 12 år, må han ta kurs hos dere?

Nei, ditt barn må ikke ta kurs dersom han allerede er medlem. Barn over 12 år som blir medlem, må ta kurs.

*Se mer*

###### Har stått lenge på venteliste og har mottat invitasjon til kurs som ikke passet med min timeplan. Hva gjør jeg?

Du må takke ja til invitasjonen du får fra oss og møte opp på kurs. I ditt tilfelle hvor du har måttet si nei til kurs, kan alternativ være å møte opp på en kursdag og se om ikke det er ledig plass, fra andre som har blitt forhindret i å komme.

==Hva med betaling i et slikt tilfelle?== 

==Hva om det kommer 4 stykker uanmeldt på kurs og så er det 2 plasser ledig. Hvordan prioriterer vi? Alfabetisk på etternavn? Fødselsdato? Dato for påmelding eller dato for beskjed om at en ikke kan komme på kurs?==

*Se mer*

###### Ønsker spesialbehandling for `yyy` fordi vi er en organisasjon type `xxx`. Hva gjør vi?

Vi kan ikke gi spesielle privelegier til spesielle grupper. Alle som skal bruke utstyret hos klubben må være medlemmer. Å være medlem innebærer at man har tatt et introkurs hos oss. Introkurset har man for å lære om hvordan man håndterer utstyret på klubben. Det innebefatter også en forsikring i medlemsskapet.

*Se mer*

###### Blir det satt opp flere obligatoriske kurs før sommeren?

Det blir satt opp så mange kurs som vi har kapasitet til. Er du på mailinglisten vår vil du få svar så snart det er ledig plasser.

*Se mer*

==Mener man før sommeren begynner? Fellesferien? Når eposten er skrevet siste dag i  Mai, blir vel svaret nei.==

###### Samboeren min og jeg har satt oss på venteliste til introkurset hos dere. Har dere noen formening om når vi kan ta kurset slik at vi kan begynne å leie kajakk? 

Vi får dessverre ikke sjekket hver enkelt på ventelisten.  Vi har foreløpig planlagt 20 introkurs, og vurderer å sette opp flere. Invitasjonene går videre til de som har vært på ventelisten sortert etter tid. 

*Se mer*

###### Jeg har vært på introduksjonskurs, og nå lurer jeg på hvordan jeg går frem for å få medlemskortet. 

Du må registrere deg på www.minidrett.no, søke opp Nøklevann ro- og padleklubb og be om medlemskap. Så snart du har gjort, vil vi få en forespørsel på vårt system hvor vi vil godkjenne medlemskapet etter en sjekk på om betalingen er i orden. Har du barn som du ønsker å melde inn, må du også be om medlemskap for disse på www.minidrett.no.

*Se mer*

######  Jeg har meldt meg på kurs, men innser at jeg har meldt meg på feil kurs. Kan jeg få refundert pengene?

Kursleder kan sørge for å melde deg av og refundere kursavgiften, slik at kursplasser blir frigjort for andre deltagere.

*Se mer*

==Jeg tror det finnes et valg på [deltager.no](http://deltager.no/) der en automatisk kan refundere påmeldings-avgiften, slik at dette går greit.==

###### Jeg har en venn som ønsker å bli medlem. Jeg er selv medlem i klubben.  Hvordan kan jeg melde henne på et introduksjons-kurs som jeg kan gi henne i gave?

Hun må selv sette seg på liste for introduksjons, så får hun etterhvert epost om kurs, beskrevet her: <http://nrpk.no/om-oss/bli-medlem/>

==Spørsmålet her er jo egentlig om andre kan melde deg på kurs==

###### Jeg har en venn som hverken bruker epost eller internet. Hvordan kan han bli medlem?

 Vi kan sende henne en faktura i posten. 

==Så gjenstår www.minidrett.no, hvordan gjøre dette for *internett-neophytes*?==

###### Jeg er medlem i en annen klubb, kan jeg bli medlem hos dere uten å ta introduksjons-kurset?

Nei, alle nye medlemmer må ta introduksjons-kurset.

*Se mer her <http://nrpk.no/om-oss/bli-medlem/>*

###### Vårt barn ble medlem i fjor, men i år finner vi ikke faktura for ham. Hva gjør vi?

Når kontingent for `2018` blir fakturert, og du ikke har fått noe epost-varsel, kan du gå inn på MinIdrett og se under "Medlemskap", der vil du finne beløp til betaling lengst oppe til høyre.

*Se mer*

